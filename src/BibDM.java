import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;
public class BibDM{

    /**
     * Ajoute deux entiers
     * @param a le premier entier à ajouter
     * @param b le deuxieme entier à ajouter
     * @return la somme des deux entiers
     */
    public static Integer plus(Integer a, Integer b){
        return a+b;
    }


    /**
     * Renvoie la valeur du plus petit élément d'une liste d'entiers
     * VOUS DEVEZ LA CODER SANS UTILISER COLLECTIONS.MIN (i.e. vous devez le faire avec un for)
     * @param liste
     * @return le plus petit élément de liste
     */
    public static Integer min(List<Integer> liste){
        if(liste.size()!=0){
            Integer mini= liste.get(0);
            for(Integer elem : liste){
                if(elem < mini){
                    mini = elem;
            }
        }
        return mini;
        }  
        else{
            return null;
        }
    }


    
    /**
     * Teste si tous les élements d'une liste sont plus petits qu'une valeure donnée
     * @param valeur 
     * @param liste
     * @return true si tous les elements de liste sont plus grands que valeur.
     */
    public static<T extends Comparable<? super T>> boolean plusPetitQueTous( T valeur , List<T> liste){
            List<T> copie = new ArrayList<T>(liste);
            copie.add(valeur);
            if(Collections.min(copie)==valeur){
                return true;
            }
            return false;
    }
    



    /**
     * Intersection de deux listes données par ordre croissant.
     * @param liste1 une liste triée
     * @param liste2 une liste triée
     * @return une liste triée avec les éléments communs à liste1 et liste2
     */
    public static <T extends Comparable<? super T>> List<T> intersection(List<T> liste1, List<T> liste2){
        List<T> liste3 = new ArrayList<T>();
        
        if(liste1.size()==0 || liste2.size()==0){
            return liste3;
        }
        else{

        if(Collections.min(liste2).compareTo(Collections.max(liste1))>0){
            return liste3;
        }
        
        else{
        
        for(T elem : liste1){
            for(T elem2 : liste2){
                if(elem == elem2){
                    if(liste3.contains(elem)==false){
                        liste3.add(elem);
                    }
                    
                }
            }
        }
        Collections.sort(liste3);         
        return liste3;
    }
}

}


    /**
     * Découpe un texte pour obtenir la liste des mots le composant. texte ne contient que des lettres de l'alphabet et des espaces.
     * @param texte une chaine de caractères
     * @return une liste de mots, correspondant aux mots de texte.
     */
    public static List<String> decoupe(String texte){
        List<String> listeDesMots = new ArrayList<String>();
        String[] sansEspaces = texte.split(" ");
        for(int i=0; i< sansEspaces.length; i++){
            if(!sansEspaces[i].equals("")){
                listeDesMots.add(sansEspaces[i]);
            }
        }
        return listeDesMots;
    }


    /**
     * Renvoie le mot le plus présent dans un texte.
     * @param texte une chaine de caractères
     * @return le mot le plus présent dans le texte. En cas d'égalité, re   nvoyer le plus petit dans l'ordre alphabétique
     */

    public static String motMajoritaire(String texte){
        Integer nombre = null;
        String mot = ("");
        Integer compt = null;
        List<String>listeDesMots = new ArrayList<>();
        String[] espace = texte.split(" ");
        for(int i=0; i< espace.length;++i){
            if(! espace[i].equals("")){
                listeDesMots.add(espace[i]);
            }
        }
        Collections.sort(listeDesMots);
        for(String m:listeDesMots){
            compt = Collections.frequency(listeDesMots,m);
            if(nombre == null || compt > nombre){
                nombre = Collections.frequency(listeDesMots,m);
                mot = m;
            }
            if(nombre > listeDesMots.size()/2 ){
                return mot;
            }

        }
        if( mot.length() > 0){ 
            return mot; 
        }
        return null;

    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de ( et de )
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ()) est mal parenthèsée et (())() est bien parenthèsée.
     */
    public static boolean bienParenthesee(String chaine){
        int ouverte = 0;
        int fermee = 0;
        for (int i=0 ; i<chaine.length() ; i++){
            if (chaine.charAt(i) == '(')
            ouverte += 1;
            else if (chaine.charAt(i) == ')')
            fermee += 1;
            if (fermee > ouverte)
            return false;
        }
        if (fermee != ouverte)
            return false;
        return true;
    }
    
    /**
     * Permet de tester si une chaine est bien parenthesée
     * @param chaine une chaine de caractères composée de (, de  ), de [ et de ]
     * @return true si la chaine est bien parentjèsée et faux sinon. Par exemple ([)] est mal parenthèsée alors ue ([]) est bien parenthèsée.
     */
    public static boolean bienParentheseeCrochets(String chaine){
        int comptParen = 0;
        int comptCro = 0;
        int testSiegales = 0;
        for(int i=0; i<chaine.length();i++){
            if(chaine.charAt(i)== '('){
                comptParen+=1;
            }
            else if(chaine.charAt(i)==')'){
                if(comptParen <= 0){
                    return false;
                }
                else{
                    comptParen -= 1;
                }

            }
            else if(chaine.charAt(i)=='['){
                comptCro+=1;
                testSiegales=comptParen;
            }
            else if(chaine.charAt(i)==']'){
                if(comptParen==testSiegales){
                    comptCro -= 1;
                }
                else{
                    return false;
                }
            }
        }
        return comptParen == 0 && comptCro == 0;
    }





    /**
     * Recherche par dichtomie d'un élément dans une liste triée
     * @param liste, une liste triée d'entiers
     * @param valeur un entier
     * @return true si l'entier appartient à la liste.
     */
    public static boolean rechercheDichotomique(List<Integer> liste, Integer valeur){
        if (liste.size() > 0) {
            int centreListe;
            int maxiListe = liste.size();
            int miniListe = 0;

            while (miniListe < maxiListe) {
                centreListe = ((miniListe + maxiListe) / 2);
                if (valeur.compareTo(liste.get(centreListe)) == 0) {
                    return true;
                } else if (valeur.compareTo(liste.get(centreListe))>0) {
                    miniListe = centreListe + 1;
                } else {
                    maxiListe = centreListe;
                }
            }
        }
        return false;

    }




}
